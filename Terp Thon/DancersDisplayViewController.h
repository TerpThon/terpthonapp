//
//  DancersDisplayViewController.h
//  Terp Thon
//
//  Created by Tyler Lundfelt on 1/16/14.
//  Copyright (c) 2014 Tyler Lundfelt. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DancersDisplayViewController : UIViewController<UITableViewDelegate,UITableViewDataSource>
@property (weak, nonatomic) IBOutlet UISegmentedControl *DancerSegControl;
@property (weak, nonatomic) IBOutlet UITableView *DancerTableView;

- (IBAction)dismissDancersDisplayViewController;
- (IBAction)reloadTable:(id)sender;

@end
