//
//  main.m
//  Terp Thon
//
//  Created by Tyler Lundfelt on 9/18/13.
//  Copyright (c) 2013 Tyler Lundfelt. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "TTAAppDelegate.h"

int main(int argc, char *argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([TTAAppDelegate class]));
    }
}
