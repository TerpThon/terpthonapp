//
//  AppDataContainer.h
//  Terp Thon
//
//  Created by Tyler Lundfelt on 1/15/14.
//  Copyright (c) 2014 Tyler Lundfelt. All rights reserved.
//

#import <Foundation/Foundation.h>


@interface AppDataContainer : NSObject

@property(nonatomic,retain)NSString             *participant_ID;
@property(nonatomic,retain)NSMutableArray       *participants;
@property(nonatomic,retain)NSMutableDictionary  *this_participant;
@property(nonatomic,retain)NSMutableArray       *this_participant_donations;
@property(nonatomic,retain)NSMutableDictionary  *event;
@property(nonatomic,retain)NSMutableArray       *teams;
@property(nonatomic,retain)NSMutableArray       *this_participant_teammates;
@property(nonatomic,retain)NSMutableArray       *color_wars_data;

+(AppDataContainer*)getInstance;
-(void)getDataFromParticipantID;
@end
