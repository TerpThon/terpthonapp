//
//  ThermSlider.h
//  Terp Thon
//
//  Created by Tyler Lundfelt on 1/29/14.
//  Copyright (c) 2014 Tyler Lundfelt. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ThermSlider : UISlider

@end
