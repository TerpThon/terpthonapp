//
//  AuxUIWebViewController.m
//  Terp Thon
//
//  Created by Tyler Lundfelt on 1/9/14.
//  Copyright (c) 2014 Tyler Lundfelt. All rights reserved.
//

#import "AuxUIWebViewController.h"

@interface AuxUIWebViewController ()

@end

@implementation AuxUIWebViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    
    [_viewer setContentMode:UIViewContentModeScaleAspectFit];
    [_viewer setAutoresizingMask:UIViewAutoresizingFlexibleHeight | UIViewAutoresizingFlexibleWidth];
    [_viewer setScalesPageToFit:YES];
    
    NSURL *filePathURL;
    if (_contentBundle != nil) {
        filePathURL = [NSURL fileURLWithPath:[_contentBundle pathForResource:_contentName ofType:_contentType]];
        _webNavBar.hidden = YES;
    } else {
        filePathURL = [NSURL URLWithString:_contentName];
    }

    [_viewer loadRequest:[NSURLRequest requestWithURL:filePathURL]];

}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)dismissAuxUIWebViewController
{
    [self dismissViewControllerAnimated:YES completion:nil];
}

@end
