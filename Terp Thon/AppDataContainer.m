//
//  AppDataContainer.m
//  Terp Thon
//
//  Created by Tyler Lundfelt on 1/15/14.
//  Copyright (c) 2014 Tyler Lundfelt. All rights reserved.
//

#import "AppDataContainer.h"

@implementation AppDataContainer
@synthesize participant_ID;
@synthesize participants;
@synthesize this_participant;
@synthesize this_participant_donations;
@synthesize event;
@synthesize teams;
@synthesize this_participant_teammates;
@synthesize color_wars_data;

static AppDataContainer *instance =nil;

+(AppDataContainer *)getInstance
{
    @synchronized(self)
    {
        if(instance==nil)
        {
            instance= [AppDataContainer new];
            
            instance.participant_ID = nil;
            instance.event = nil;
            instance.this_participant = nil;
            instance.this_participant_donations = nil;
            instance.this_participant_teammates = nil;
            instance.participants = nil;
            instance.teams = nil;
            instance.color_wars_data = nil;
        }
    }
    return instance;
}

-(void)getDataFromParticipantID
{
    
    if (instance==nil){

        return;
    }
    
    NSString *append = @"";
    
    if (instance.participant_ID) {
        append = instance.participant_ID;
    }
    
    instance.event = nil;
    instance.this_participant = nil;
    instance.this_participant_donations = nil;
    instance.this_participant_teammates = nil;
    instance.participants = nil;
    instance.teams = nil;
    instance.color_wars_data = nil;
    
    
    NSURL *url = [[NSURL alloc] initWithString:[@"http://www.terpthon.org/TTAPP_get_donor_data.php?participant_id=" stringByAppendingString:append]];
    NSMutableURLRequest *request  = [[NSMutableURLRequest alloc] initWithURL:url];
    
    [request setHTTPMethod:@"GET"];
    
    NSError *requestError;
    NSURLResponse *urlResponse = nil;
    
    NSData *rawJSON = [NSURLConnection sendSynchronousRequest:request returningResponse:&urlResponse error:&requestError];
    
    NSError *jsonError;
    
    if (rawJSON == nil || [NSJSONSerialization isValidJSONObject:rawJSON]) {
        
        UIAlertView *failure_alert = [[UIAlertView alloc] initWithTitle:@"Connection Error" message:@"Internet connection could not be established.\n User data retrieval failed." delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil];
        [failure_alert show];
        return;
    }

    NSArray *data = [NSJSONSerialization JSONObjectWithData:rawJSON options:NSJSONReadingMutableContainers error:&jsonError];
    
    
    if ([[data objectAtIndex:0] isKindOfClass:[NSDictionary class]]) {
        instance.event = [[NSMutableDictionary alloc] initWithDictionary:(NSDictionary *)[data objectAtIndex:0]];
    }
    
    if ([[data objectAtIndex:1] isKindOfClass:[NSDictionary class]]) {
        
        instance.this_participant = [[NSMutableDictionary alloc] initWithDictionary:(NSDictionary *)[data objectAtIndex:1]];
        
    } else if (instance.participant_ID && !([instance.participant_ID isEqualToString:@""])) {

        UIAlertView *dancerID_alert = [[UIAlertView alloc] initWithTitle:@"DancerID Error" message:@"DancerID was not found.\n Click the settings gear icon on the home page to enter a DancerID." delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil];
        [dancerID_alert show];
        
    }
    
    if ([[data objectAtIndex:2] isKindOfClass:[NSArray class]]) {
    instance.this_participant_donations = [[NSMutableArray alloc] initWithArray:[data objectAtIndex:2]];
    }
    
    if ([[data objectAtIndex:3] isKindOfClass:[NSArray class]]) {
        instance.this_participant_teammates = [[NSMutableArray alloc] initWithArray:[data objectAtIndex:3]];
    }
    
    if ([[data objectAtIndex:4] isKindOfClass:[NSArray class]]) {
        instance.participants = [[NSMutableArray alloc] initWithArray:[data objectAtIndex:4]];
    }
    
    if ([[data objectAtIndex:5] isKindOfClass:[NSArray class]]) {
        instance.teams = [[NSMutableArray alloc] initWithArray:[data objectAtIndex:5]];
    }
    
    if ([[data objectAtIndex:6] isKindOfClass:[NSArray class]]) {
        instance.color_wars_data = [[NSMutableArray alloc] initWithArray:[data objectAtIndex:6]];
    }
    
}


@end
