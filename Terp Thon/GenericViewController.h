//
//  GenericViewController.h
//  Terp Thon
//
//  Created by Tyler Lundfelt on 1/17/14.
//  Copyright (c) 2014 Tyler Lundfelt. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface GenericViewController : UIViewController

@end
