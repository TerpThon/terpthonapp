//
//  MediaViewController.m
//  Terp Thon
//
//  Created by Tyler Lundfelt on 1/9/14.
//  Copyright (c) 2014 Tyler Lundfelt. All rights reserved.
//

#import "MediaViewController.h"
#import "AuxUIWebViewController.h"
#import "AppDataContainer.h"

@interface MediaViewController ()

@end

@implementation MediaViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    self.view.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"triangular.png"]];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender{
    
    if([segue.identifier isEqualToString:@"facebookSegue"]){
        
        AuxUIWebViewController *controller = (AuxUIWebViewController *)segue.destinationViewController;
        controller.contentName = @"https://www.facebook.com/FTKTerpThon";
        controller.contentType = @"";
        controller.contentBundle = nil;
        
    } else if ([segue.identifier isEqualToString:@"twitterSegue"]) {
        
        AuxUIWebViewController *controller = (AuxUIWebViewController *)segue.destinationViewController;
        controller.contentName = @"https://twitter.com/TerpThon";
        controller.contentType = @"";
        controller.contentBundle = nil;
        
    } else if ([segue.identifier isEqualToString:@"youtubeSegue"]){
        
        AuxUIWebViewController *controller = (AuxUIWebViewController *)segue.destinationViewController;
        controller.contentName = @"http://www.youtube.com/user/terpthonUMD/videos";
        controller.contentType = @"";
        controller.contentBundle = nil;
        
    } else if ([segue.identifier isEqualToString:@"instagramSegue"]){
        
        AuxUIWebViewController *controller = (AuxUIWebViewController *)segue.destinationViewController;
        controller.contentName = @"http://instagram.com/terpthon#";
        controller.contentType = @"";
        controller.contentBundle = nil;
        
    }
}

@end
