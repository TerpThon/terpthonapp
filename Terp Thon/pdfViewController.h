//
//  pdfViewController.h
//  Terp Thon
//
//  Created by Tyler Lundfelt on 1/3/14.
//  Copyright (c) 2014 Tyler Lundfelt. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface pdfViewController : UIViewController
@property (weak, nonatomic) IBOutlet UIWebView *pdfViewer;
@property (nonatomic) NSString *pdfName;
- (IBAction)dismissPDFViewController;

@end
