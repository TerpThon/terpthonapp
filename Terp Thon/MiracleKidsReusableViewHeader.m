//
//  MiracleKidsReusableViewHeader.m
//  Terp Thon
//
//  Created by Tyler Lundfelt on 1/3/14.
//  Copyright (c) 2014 Tyler Lundfelt. All rights reserved.
//

#import "MiracleKidsReusableViewHeader.h"

@implementation MiracleKidsReusableViewHeader

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

@end
