//
//  TerpThonTableViewCell.m
//  Terp Thon
//
//  Created by Tyler Lundfelt on 2/5/14.
//  Copyright (c) 2014 Tyler Lundfelt. All rights reserved.
//

#import "TerpThonTableViewCell.h"

@implementation TerpThonTableViewCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
