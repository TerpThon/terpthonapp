//
//  pdfViewController.m
//  Terp Thon
//
//  Created by Tyler Lundfelt on 1/3/14.
//  Copyright (c) 2014 Tyler Lundfelt. All rights reserved.
//

#import "pdfViewController.h"

@interface pdfViewController ()

@end

@implementation pdfViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    
    [_pdfViewer setContentMode:UIViewContentModeScaleAspectFit];
    [_pdfViewer setAutoresizingMask:UIViewAutoresizingFlexibleHeight | UIViewAutoresizingFlexibleWidth];
    [_pdfViewer setScalesPageToFit:YES];
    
    NSString *filePath = [[NSBundle mainBundle] pathForResource:_pdfName ofType:@"pdf"];
    NSURL *filePathURL = [NSURL fileURLWithPath:filePath];
    [_pdfViewer loadRequest:[NSURLRequest requestWithURL:filePathURL]];

    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)dismissPDFViewController
{
    [self dismissViewControllerAnimated:YES completion:nil];
    
}

@end
