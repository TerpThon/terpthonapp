//
//  HomeViewController.h
//  Terp Thon
//
//  Created by Tyler Lundfelt on 1/10/14.
//  Copyright (c) 2014 Tyler Lundfelt. All rights reserved.
//

#import <UIKit/UIKit.h>


@interface HomeViewController : UIViewController
- (IBAction)refresh:(id)sender;
@end
