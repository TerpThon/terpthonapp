//
//  DonorDriveViewController.h
//  Terp Thon
//
//  Created by Tyler Lundfelt on 1/7/14.
//  Copyright (c) 2014 Tyler Lundfelt. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ThermView.h"

@interface DonorDriveViewController : UIViewController <UITableViewDelegate, UITableViewDataSource>
@property (weak, nonatomic) IBOutlet ThermView *overallProgress;
@property (weak, nonatomic) IBOutlet ThermView *teamProgress;
@property (weak, nonatomic) IBOutlet ThermView *personalProgress;
@property (weak, nonatomic) IBOutlet UITableView *donorsTableView;
@property (weak, nonatomic) IBOutlet UILabel *overallRaisedLabel;
@property (weak, nonatomic) IBOutlet UILabel *teamRaisedLabel;
@property (weak, nonatomic) IBOutlet UILabel *personalRaisedLabel;
@property (weak, nonatomic) IBOutlet UILabel *overallGoalLabel;
@property (weak, nonatomic) IBOutlet UILabel *teamGoalLabel;
@property (weak, nonatomic) IBOutlet UILabel *personalGoalLabel;
@property (weak, nonatomic) IBOutlet UILabel *teamNameLabel;
@property (weak, nonatomic) IBOutlet UILabel *personalNameLabel;

-(void)setValues;

@end
