//
//  SettingsViewController.m
//  Terp Thon
//
//  Created by Tyler Lundfelt on 1/19/14.
//  Copyright (c) 2014 Tyler Lundfelt. All rights reserved.
//

#import "SettingsViewController.h"
#import "AppDataContainer.h"

@interface SettingsViewController ()
{
    AppDataContainer *data;
}
@end

@implementation SettingsViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    self.view.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"triangular.png"]];

    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    if (![[defaults objectForKey:@"participant_id"] isEqualToString:@""]) {
        _dancerIDTextBox.text = [defaults objectForKey:@"participant_id"];
    }
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    [textField resignFirstResponder];
    return NO;
}

- (IBAction)dismissSettingsViewController:(id)sender
{
    if (sender == _doneButton)
    {
        NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
        
        NSString *key = @"participant_id"; // the key for the data
        
        [defaults setObject:[NSString stringWithString:_dancerIDTextBox.text] forKey:key];
        [defaults synchronize]; // this method is optional
        
        data = [AppDataContainer getInstance];
        data.participant_ID = [NSString stringWithString:[[NSUserDefaults standardUserDefaults] stringForKey:key]];
        [data getDataFromParticipantID];

    }
    
    [self dismissViewControllerAnimated:YES completion:nil];
}
@end
