//
//  SettingsViewController.h
//  Terp Thon
//
//  Created by Tyler Lundfelt on 1/19/14.
//  Copyright (c) 2014 Tyler Lundfelt. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SettingsViewController : UIViewController<UITextFieldDelegate>
@property (weak, nonatomic) IBOutlet UIButton *doneButton;
@property (weak, nonatomic) IBOutlet UITextField *dancerIDTextBox;

-(IBAction)dismissSettingsViewController:(id)sender;

@end
