//
//  EventViewController.m
//  Terp Thon
//
//  Created by Tyler Lundfelt on 1/15/14.
//  Copyright (c) 2014 Tyler Lundfelt. All rights reserved.
//

#import "EventViewController.h"
#import "AppDataContainer.h"

@interface EventViewController ()
{
    AppDataContainer *data;
    NSNumberFormatter *formatter;

    
}
@end

@implementation EventViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    self.view.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"triangular.png"]];
    
    formatter = [[NSNumberFormatter alloc] init];
    [formatter setNumberStyle:NSNumberFormatterCurrencyStyle];
    [formatter setMaximumFractionDigits:0];
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:YES];
    
    data = [AppDataContainer getInstance];
    
    if (data.event){
    
        [_goalLabel setText:[formatter stringForObjectValue:[NSNumber numberWithInt:[[data.event objectForKey:@"fundraisinggoal"] intValue]]]];
        [_raisedLabel setText:[formatter stringForObjectValue:[NSNumber numberWithInt:[[data.event objectForKey:@"sumdonations"] intValue]]]];
    }
    
    NSString *k1 = @"team";
    NSString *k2 = @"points";
    
    
    float maxPoints = 500;
    float relMax = 0;

    NSMutableArray *arr = [[NSMutableArray alloc] init];
    
    if (data.color_wars_data) {
        arr = data.color_wars_data;
    }
    
        for (int j = 0; j < [arr count]; j++) {
            
            if ([[arr objectAtIndex:j] isKindOfClass:[NSMutableDictionary class]]) {
                NSMutableDictionary *t = [arr objectAtIndex:j];
                if ([[t objectForKey:k2] intValue] > relMax) {
                    relMax = [[t objectForKey:k2] intValue];
                }
            }
        }
        
        if (relMax > 500){
            maxPoints = relMax * 1.33;
        }
        
        for (int i = 0; i < [arr count]; i++) {

            NSMutableDictionary *temp = [arr objectAtIndex:i];

            if (i==0) {
                _team1Label.text = [temp objectForKey:k1];
                _team1Bar.color = [EventViewController determineColor:temp];
                _team1Bar.value = [[temp objectForKey:k2] intValue]/maxPoints;
                _team1Points.text = [temp objectForKey:k2];
                [_team1Bar setNeedsDisplay];
            } else if (i==1) {
                _team2Label.text = [temp objectForKey:k1];
                _team2Bar.color = [EventViewController determineColor:temp];
                _team2Bar.value = [[temp objectForKey:k2] intValue]/maxPoints;
                _team2Points.text = [temp objectForKey:k2];
                [_team2Bar setNeedsDisplay];
            } else if (i==2) {
                _team3Label.text = [temp objectForKey:k1];
                _team3Bar.color = [EventViewController determineColor:temp];
                _team3Bar.value = [[temp objectForKey:k2] intValue]/maxPoints;
                _team3Points.text = [temp objectForKey:k2];
                [_team3Bar setNeedsDisplay];
            } else if (i==3) {
                _team4Label.text = [temp objectForKey:k1];
                _team4Bar.color = [EventViewController determineColor:temp];
                _team4Bar.value = [[temp objectForKey:k2] intValue]/maxPoints;
                _team4Points.text = [temp objectForKey:k2];
                [_team4Bar setNeedsDisplay];
            } else if (i==4) {
                _team5Label.text = [temp objectForKey:k1];
                _team5Bar.color = [EventViewController determineColor:temp];
                _team5Bar.value = [[temp objectForKey:k2] intValue]/maxPoints;
                _team5Points.text = [temp objectForKey:k2];
                [_team5Bar setNeedsDisplay];
            } else if (i==5) {
                _team6Label.text = [temp objectForKey:k1];
                _team6Bar.color = [EventViewController determineColor:temp];
                _team6Bar.value = [[temp objectForKey:k2] intValue]/maxPoints;
                _team6Points.text = [temp objectForKey:k2];
                [_team6Bar setNeedsDisplay];
            } else {
                break;
            }
        }
    

}

+(UIColor *)determineColor:(NSMutableDictionary *)team{
    
    UIColor *toReturn = [UIColor blackColor];
    
    float red,green,blue;
    
    red = [[team objectForKey:@"red"] floatValue]/255.0;
    green = [[team objectForKey:@"green"] floatValue]/255.0;
    blue  = [[team objectForKey:@"blue"] floatValue]/255.0;
    
    
    if ( red || green || blue ) {
        toReturn = [UIColor colorWithRed:red green:green blue:blue alpha:.90];
    }
    
    return toReturn;
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
