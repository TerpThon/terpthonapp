//
//  MiracleKidsCell.h
//  Terp Thon
//
//  Created by Tyler Lundfelt on 12/26/13.
//  Copyright (c) 2013 Tyler Lundfelt. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MiracleKidsCell : UICollectionViewCell
@property (weak, nonatomic) IBOutlet UIImageView *miracleKidsImageView;
@property (weak, nonatomic) IBOutlet UIButton *pdfTransitionButton;

@end
