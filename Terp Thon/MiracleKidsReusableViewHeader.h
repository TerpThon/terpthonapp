//
//  MiracleKidsReusableViewHeader.h
//  Terp Thon
//
//  Created by Tyler Lundfelt on 1/3/14.
//  Copyright (c) 2014 Tyler Lundfelt. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MiracleKidsReusableViewHeader : UICollectionReusableView
@property (weak, nonatomic) IBOutlet UILabel *title;

@end
