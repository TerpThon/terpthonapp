//
//  TerpThonTableViewCell.h
//  Terp Thon
//
//  Created by Tyler Lundfelt on 2/5/14.
//  Copyright (c) 2014 Tyler Lundfelt. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TerpThonTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *nameLabel;
@property (weak, nonatomic) IBOutlet UILabel *subLabel;
@property (weak, nonatomic) IBOutlet UILabel *moneyLabel;

@end
