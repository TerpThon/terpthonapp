//
//  TeamsDisplayViewController.h
//  Terp Thon
//
//  Created by Tyler Lundfelt on 1/17/14.
//  Copyright (c) 2014 Tyler Lundfelt. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TeamsDisplayViewController : UIViewController<UITableViewDelegate,UITableViewDataSource>
@property (weak, nonatomic) IBOutlet UISegmentedControl *TeamSegControl;
@property (weak, nonatomic) IBOutlet UITableView *TeamTableView;

- (IBAction)dismissTeamsDisplayViewController;
- (IBAction)reloadTable:(id)sender;

@end
