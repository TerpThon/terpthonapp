//
//  DancersDisplayViewController.m
//  Terp Thon
//
//  Created by Tyler Lundfelt on 1/16/14.
//  Copyright (c) 2014 Tyler Lundfelt. All rights reserved.
//

#import "DancersDisplayViewController.h"
#import "AppDataContainer.h"
#import "TerpThonTableViewCell.h"

@interface DancersDisplayViewController ()
{
    AppDataContainer *data;
    NSArray *dancersAll;
    NSArray *dancersTeam;
    NSNumberFormatter *formatter;
}

@end

@implementation DancersDisplayViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    [_DancerSegControl setTintColor:[UIColor clearColor]];
    [_DancerSegControl setTintColor:self.view.tintColor];
    
    formatter = [[NSNumberFormatter alloc] init];
    [formatter setNumberStyle:NSNumberFormatterCurrencyStyle];
    
    data = [AppDataContainer getInstance];
    
    dancersAll = data.participants;
    dancersTeam = data.this_participant_teammates;
    
    dancersAll = [dancersAll sortedArrayUsingComparator:^(NSDictionary *item1, NSDictionary *item2) {
        NSString *amount1 = [item1 objectForKey:@"participantsumdonations"];
        NSString *amount2 = [item2 objectForKey:@"participantsumdonations"];
        return [amount2 compare:amount1 options:NSNumericSearch];
    }];
    
    dancersTeam = [dancersTeam sortedArrayUsingComparator:^(NSDictionary *item1, NSDictionary *item2) {
        NSString *amount1 = [item1 objectForKey:@"participantsumdonations"];
        NSString *amount2 = [item2 objectForKey:@"participantsumdonations"];
        return [amount2 compare:amount1 options:NSNumericSearch];
    }];

    [_DancerTableView reloadData];
}

-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:YES];
    
    data = [AppDataContainer getInstance];
    
    dancersAll = data.participants;
    dancersTeam = data.this_participant_teammates;
    
    dancersAll = [dancersAll sortedArrayUsingComparator:^(NSDictionary *item1, NSDictionary *item2) {
        NSString *amount1 = [item1 objectForKey:@"participantsumdonations"];
        NSString *amount2 = [item2 objectForKey:@"participantsumdonations"];
        return [amount2 compare:amount1 options:NSNumericSearch];
    }];
    
    dancersTeam = [dancersTeam sortedArrayUsingComparator:^(NSDictionary *item1, NSDictionary *item2) {
        NSString *amount1 = [item1 objectForKey:@"participantsumdonations"];
        NSString *amount2 = [item2 objectForKey:@"participantsumdonations"];
        return [amount2 compare:amount1 options:NSNumericSearch];
    }];
    
    [_DancerTableView reloadData];
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    
    NSString *cellIdentifier = @"cellReuseID";
    TerpThonTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    
    NSDictionary *participantDictionary;
    /*
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:cellIdentifier];
    }*/
    
    if (_DancerSegControl.selectedSegmentIndex == 0)
    {
        participantDictionary = [dancersAll objectAtIndex:indexPath.item];
    }
    else
    {
        participantDictionary = [dancersTeam objectAtIndex:indexPath.item];
    }

    
    NSString *leftLabel =
        [NSString stringWithFormat:@"%ld. %@ %@",
         ((long)indexPath.item + 1),
         [participantDictionary objectForKey:@"participantfirstname"],
         [participantDictionary objectForKey:@"participantlastname"]];
         
    NSString *rightLabel = [formatter stringForObjectValue:[NSNumber numberWithFloat:[[participantDictionary objectForKey:@"participantsumdonations"]floatValue]]];
    
  
    cell.nameLabel.text = leftLabel;
    cell.moneyLabel.text = rightLabel;
    cell.subLabel.text = [participantDictionary objectForKey:@"teamname"];
    
    return cell;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    
    if (_DancerSegControl.selectedSegmentIndex == 0)
    {
        if (dancersAll)
        {
            return [dancersAll count];
        }
        else
        {
            return 0;
        }
    }
    else
    {
        if (dancersTeam)
        {
            return [dancersTeam count];
        }
        else
        {
            return 0;
        }
    }
    
}

- (IBAction)dismissDancersDisplayViewController
{
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (IBAction)reloadTable:(id)sender
{
    [_DancerTableView reloadData];
}

@end
