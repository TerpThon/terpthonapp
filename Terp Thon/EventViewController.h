//
//  EventViewController.h
//  Terp Thon
//
//  Created by Tyler Lundfelt on 1/15/14.
//  Copyright (c) 2014 Tyler Lundfelt. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ThermView.h"

@interface EventViewController : UIViewController
@property (weak, nonatomic) IBOutlet UILabel *raisedLabel;
@property (weak, nonatomic) IBOutlet UILabel *goalLabel;
@property (weak, nonatomic) IBOutlet UILabel *team1Label;
@property (weak, nonatomic) IBOutlet UILabel *team2Label;
@property (weak, nonatomic) IBOutlet UILabel *team3Label;
@property (weak, nonatomic) IBOutlet UILabel *team4Label;
@property (weak, nonatomic) IBOutlet UILabel *team5Label;
@property (weak, nonatomic) IBOutlet UILabel *team6Label;
@property (weak, nonatomic) IBOutlet ThermView *team1Bar;
@property (weak, nonatomic) IBOutlet ThermView *team2Bar;
@property (weak, nonatomic) IBOutlet ThermView *team3Bar;
@property (weak, nonatomic) IBOutlet ThermView *team4Bar;
@property (weak, nonatomic) IBOutlet ThermView *team5Bar;
@property (weak, nonatomic) IBOutlet ThermView *team6Bar;
@property (weak, nonatomic) IBOutlet UILabel *team1Points;
@property (weak, nonatomic) IBOutlet UILabel *team2Points;
@property (weak, nonatomic) IBOutlet UILabel *team3Points;
@property (weak, nonatomic) IBOutlet UILabel *team4Points;
@property (weak, nonatomic) IBOutlet UILabel *team5Points;
@property (weak, nonatomic) IBOutlet UILabel *team6Points;

@end
