//
//  ThermView.h
//  Terp Thon
//
//  Created by Tyler Lundfelt on 1/29/14.
//  Copyright (c) 2014 Tyler Lundfelt. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ThermView : UIView

@property float value;
@property UIColor *color;
@end
