//
//  ThermView.m
//  Terp Thon
//
//  Created by Tyler Lundfelt on 1/29/14.
//  Copyright (c) 2014 Tyler Lundfelt. All rights reserved.
//

#import "ThermView.h"

@implementation ThermView


- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}


- (void)drawRect:(CGRect)rect {
    CGContextRef c = UIGraphicsGetCurrentContext();
    
    if (!self.color) {
        // Donor Drive Therms
        self.color = [UIColor colorWithRed:(230.0/255.0) green:(0.0/255.0) blue:(0.0/255.0) alpha:0.70];
    }
    
    
    [self.color set];
    CGFloat ins = 2.0;
    CGRect r = CGRectInset(self.bounds, ins, ins);
    CGFloat radius = r.size.height / 2.0;
    CGMutablePathRef path = CGPathCreateMutable();
    CGPathMoveToPoint(path, NULL, CGRectGetMaxX(r) - radius, ins);
    CGPathAddArc(path, NULL, radius+ins, radius+ins, radius, -M_PI/2.0, M_PI/2.0, true);
    CGPathAddArc(path, NULL, CGRectGetMaxX(r) - radius, radius+ins, radius, M_PI/2.0, -M_PI/2.0, true);
    CGPathCloseSubpath(path);
    CGContextAddPath(c, path);
    CGContextSetLineWidth(c, 2);
    CGContextStrokePath(c);
    CGContextAddPath(c, path);
    CGContextClip(c);
    CGContextFillRect(c, CGRectMake(r.origin.x, r.origin.y, r.size.width * self.value, r.size.height));
    //self.transform = CGAffineTransformMakeRotation(M_PI * -0.5);

}

@end
