//
//  TTAAppDelegate.h
//  Terp Thon
//
//  Created by Tyler Lundfelt on 9/18/13.
//  Copyright (c) 2013 Tyler Lundfelt. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TTAAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
