//
//  AuxUIWebViewController.h
//  Terp Thon
//
//  Created by Tyler Lundfelt on 1/9/14.
//  Copyright (c) 2014 Tyler Lundfelt. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AuxUIWebViewController : UIViewController
@property (weak, nonatomic) IBOutlet UIWebView *viewer;
@property (weak, nonatomic) IBOutlet UINavigationBar *webNavBar;
@property (weak, nonatomic) IBOutlet UIBarButtonItem *backArrowBarButton;
@property (weak, nonatomic) IBOutlet UIBarButtonItem *forwardArrowBarButton;
@property (nonatomic) NSString *contentName;
@property (nonatomic) NSString *contentType;
@property (nonatomic) NSBundle *contentBundle;

- (IBAction)dismissAuxUIWebViewController;

@end
