//
//  TeamsDisplayViewController.m
//  Terp Thon
//
//  Created by Tyler Lundfelt on 1/17/14.
//  Copyright (c) 2014 Tyler Lundfelt. All rights reserved.
//

#import "TeamsDisplayViewController.h"
#import "AppDataContainer.h"
#import "TerpThonTableViewCell.h"

@interface TeamsDisplayViewController ()
{
    AppDataContainer *data;
    NSArray *teamsAll;
    NSArray *teamsAvg;
    NSNumberFormatter *formatter;
}

@end

@implementation TeamsDisplayViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    [_TeamSegControl setTintColor:[UIColor clearColor]];
    [_TeamSegControl setTintColor:self.view.tintColor];
    
    formatter = [[NSNumberFormatter alloc] init];
    [formatter setNumberStyle:NSNumberFormatterCurrencyStyle];
    
    data = [AppDataContainer getInstance];
    
    if (data.teams) {
        teamsAll = data.teams;
    } else {
        teamsAll = [[NSArray alloc] init];
    }
    
    teamsAll = [teamsAll sortedArrayUsingComparator:^(NSDictionary *item1, NSDictionary *item2) {
        NSString *amount1 = [item1 objectForKey:@"teamsumdonations"];
        NSString *amount2 = [item2 objectForKey:@"teamsumdonations"];
        return [amount2 compare:amount1 options:NSNumericSearch];
    }];
    
    teamsAvg = [teamsAll sortedArrayUsingComparator:^(NSDictionary *item1, NSDictionary *item2) {
        NSString *amount1 = [item1 objectForKey:@"teamaverage"];
        NSString *amount2 = [item2 objectForKey:@"teamaverage"];
        return [amount2 compare:amount1 options:NSNumericSearch];
    }];
    
    [_TeamTableView reloadData];

}

-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:YES];
    
    data = [AppDataContainer getInstance];
    
    if (data.teams) {
        teamsAll = data.teams;
    } else {
        teamsAll = [[NSArray alloc] init];
    }
    
    teamsAll = [teamsAll sortedArrayUsingComparator:^(NSDictionary *item1, NSDictionary *item2) {
        NSString *amount1 = [item1 objectForKey:@"teamsumdonations"];
        NSString *amount2 = [item2 objectForKey:@"teamsumdonations"];
        return [amount2 compare:amount1 options:NSNumericSearch];
    }];
    
    teamsAvg = [teamsAll sortedArrayUsingComparator:^(NSDictionary *item1, NSDictionary *item2) {
        NSString *amount1 = [item1 objectForKey:@"teamaverage"];
        NSString *amount2 = [item2 objectForKey:@"teamaverage"];
        return [amount2 compare:amount1 options:NSNumericSearch];
    }];
    
    [_TeamTableView reloadData];

    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    
    NSString *cellIdentifier = @"cellReuseID2";
    TerpThonTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    NSDictionary *participantDictionary;
    NSString *rightLabelKey;
    /*
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:cellIdentifier];
    }*/
    
    if (_TeamSegControl.selectedSegmentIndex == 0)
    {
        participantDictionary = [teamsAll objectAtIndex:indexPath.item];
        rightLabelKey = @"teamsumdonations";
    }
    else
    {
        participantDictionary = [teamsAvg objectAtIndex:indexPath.item];
        rightLabelKey = @"teamaverage";
    }
    
    
    NSString *leftLabel =
    [NSString stringWithFormat:@"%ld. %@",
     ((long)indexPath.item + 1),
     [participantDictionary objectForKey:@"teamname"]];
    
    NSString *rightLabel = [formatter stringForObjectValue:[NSNumber numberWithFloat:[[participantDictionary objectForKey:rightLabelKey]floatValue]]];
    
    cell.nameLabel.text = leftLabel;
    cell.moneyLabel.text = rightLabel;
    //cell.subLabel.text = [participantDictionary objectForKey:@"teamname"];
    
    
    return cell;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    
    if (_TeamSegControl.selectedSegmentIndex == 0)
    {
        if (teamsAll)
        {
            return [teamsAll count];
        }
        else
        {
            return 0;
        }
    }
    else
    {
        if (teamsAvg)
        {
            return [teamsAvg count];
        }
        else
        {
            return 0;
        }
    }
    
}

- (IBAction)dismissTeamsDisplayViewController
{
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (IBAction)reloadTable:(id)sender
{
    [_TeamTableView reloadData];
}

@end
