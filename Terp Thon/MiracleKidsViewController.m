//
//  MiracleKidsViewController.m
//  Terp Thon
//
//  Created by Tyler Lundfelt on 12/26/13.
//  Copyright (c) 2013 Tyler Lundfelt. All rights reserved.
//

#import "MiracleKidsViewController.h"
#import "MiracleKidsCell.h"
#import "MiracleKidsReusableViewHeader.h"
#import "AuxUIWebViewController.h"

@interface MiracleKidsViewController ()
{
    NSArray *kidsDataSource;
}

@end

@implementation MiracleKidsViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    
    kidsDataSource = [[NSArray alloc] initWithObjects: @"Alexander", @"Amanda", @"Brooke",@"Carsten",@"Chase",@"Daisy",@"Ian",@"Jessica",@"Luca",@"Micah",@"Noble",@"Olivia",@"Ryan",@"Tess",@"Zoie", nil];
   [super viewDidLoad];
	// Do any additional setup after loading the view.
    self.view.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"triangular.png"]];
    
}


/*Configure cell at index*/
-(UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    NSString *currentKid = [kidsDataSource objectAtIndex:indexPath.item];
    MiracleKidsCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"reuseID" forIndexPath:indexPath];
    [[cell miracleKidsImageView] setImage:[UIImage imageNamed:[currentKid stringByAppendingString:@".jpg"]]];
    [[cell pdfTransitionButton] setTitle:currentKid forState:UIControlStateNormal];
    
    return cell;
}


/*Determine number of items in view*/
-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return [kidsDataSource count];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


/*For header*/
- (UICollectionReusableView *)collectionView:(UICollectionView *)collectionView viewForSupplementaryElementOfKind:(NSString *)kind atIndexPath:(NSIndexPath *)indexPath
{
    UICollectionReusableView *reusableview = nil;
    
    if (kind == UICollectionElementKindSectionHeader) {
        
        MiracleKidsReusableViewHeader *headerView = [collectionView dequeueReusableSupplementaryViewOfKind:UICollectionElementKindSectionHeader withReuseIdentifier:@"HeaderView" forIndexPath:indexPath];
        
        NSString *title = [[NSString alloc]initWithFormat:@"Miracle Kids"];
        headerView.title.text = title;
        //UIImage *headerImage = [UIImage imageNamed:@"header_banner.png"];
        //headerView.backgroundImage.image = headerImage;
        
        reusableview = headerView;
    }
    
    return reusableview;
}


/*Pass pdf name to viewer*/
-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender{
    if([segue.identifier isEqualToString:@"kidSegue"]){
        UIButton *theButton = sender;
        AuxUIWebViewController *controller = (AuxUIWebViewController *)segue.destinationViewController;
        controller.contentName = [theButton titleForState:UIControlStateNormal];
        controller.contentType = @"pdf";
        controller.contentBundle = [NSBundle mainBundle];
    }
}


@end
