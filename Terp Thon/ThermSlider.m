//
//  ThermSlider.m
//  Terp Thon
//
//  Created by Tyler Lundfelt on 1/29/14.
//  Copyright (c) 2014 Tyler Lundfelt. All rights reserved.
//

#import "ThermSlider.h"

@implementation ThermSlider

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

- (CGRect)trackRectForBounds:(CGRect)bounds {
    CGRect result = [super trackRectForBounds:bounds];
    result.size.height = 0;
    return result;
}

@end
