//
//  DonorDriveViewController.m
//  Terp Thon
//
//  Created by Tyler Lundfelt on 1/7/14.
//  Copyright (c) 2014 Tyler Lundfelt. All rights reserved.
//

#import "DonorDriveViewController.h"
#import "AppDataContainer.h"
#import "TerpThonTableViewCell.h"

@interface DonorDriveViewController ()
{
    AppDataContainer *data;
    NSMutableArray *donorsDataSource;
    NSDictionary *eventData;
    NSDictionary *personalData;
    NSNumberFormatter *formatter;
    NSDateFormatter *dateFormatter;
}


@end

@implementation DonorDriveViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    self.view.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"triangular.png"]];
    
    CGAffineTransform trans = CGAffineTransformMakeRotation(M_PI * -0.5);
    
    _overallProgress.transform = trans;
    _teamProgress.transform = trans;
    _personalProgress.transform = trans;
    
    formatter = [[NSNumberFormatter alloc] init];
    [formatter setNumberStyle:NSNumberFormatterCurrencyStyle];
    [formatter setMaximumFractionDigits:0];
    
    dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"yyyy-dd-mm"];
}

-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:YES];
    data = [AppDataContainer getInstance];
    
    if (data.this_participant_donations) {
        donorsDataSource = data.this_participant_donations;
    } else {
        donorsDataSource = [[NSMutableArray alloc] init];
    }
    
    if (data.event) {
        eventData = data.event;
    } else {
        eventData = [[NSDictionary alloc] init];
    }
    
    if (data.this_participant) {
        personalData = data.this_participant;
    } else {
        personalData = [[NSDictionary alloc] init];
    }
    
    donorsDataSource = [[NSMutableArray alloc] initWithArray:[donorsDataSource sortedArrayUsingComparator:^(NSDictionary *item1, NSDictionary *item2) {
        
        NSDate *d1 = [dateFormatter dateFromString:[[item1 objectForKey:@"donationdateentered"] substringToIndex:10]];
        NSDate *d2 = [dateFormatter dateFromString:[[item2 objectForKey:@"donationdateentered"] substringToIndex:10]];
        return [d2 compare:d1];
    }]];
    
    [self setValues];
}

-(void)setValues
{
    float overallGoal=0.0,overallRaised=0.0,teamGoal=0.0,teamRaised=0.0,personalGoal=0.0,personalRaised = 0.0;

    
    if (eventData) {
        if ([eventData objectForKey:@"fundraisinggoal"]) {
            overallGoal = [[eventData objectForKey:@"fundraisinggoal"] floatValue];
        }
        if ([eventData objectForKey:@"sumdonations"]) {
            overallRaised = [[eventData objectForKey:@"sumdonations"] floatValue];
        }
    }
    
    if (personalData) {
        if ([personalData objectForKey:@"teamfundraisinggoal"]) {
            teamGoal = [[personalData objectForKey:@"teamfundraisinggoal"] floatValue];
        }
        if ([personalData objectForKey:@"teamsumdonations"]) {
            teamRaised = [[personalData objectForKey:@"teamsumdonations"] floatValue];
        }
        if ([personalData objectForKey:@"participantfundraisinggoal"]) {
            personalGoal = [[personalData objectForKey:@"participantfundraisinggoal"] floatValue];
        }
        if ([personalData objectForKey:@"participantsumdonations"]) {
            personalRaised = [[personalData objectForKey:@"participantsumdonations"] floatValue];
        }
    }
    
    [_overallGoalLabel setText:[formatter stringForObjectValue:[NSNumber numberWithInt:(int)(overallGoal)]]];
    [_overallRaisedLabel setText:[formatter stringForObjectValue:[NSNumber numberWithInt:(int)(overallRaised)]]];
    
    float toSet = (overallRaised/overallGoal);
    if (toSet > 1.0) {
        toSet = 1.0;
    }
    
    _overallProgress.value = toSet;
    [_overallProgress setNeedsDisplay];

    
    [_teamGoalLabel setText:[formatter stringForObjectValue:[NSNumber numberWithInt:(int)(teamGoal)]]];
    [_teamRaisedLabel setText:[formatter stringForObjectValue:[NSNumber numberWithInt:(int)(teamRaised)]]];
    toSet = (teamRaised/teamGoal);
    if (toSet > 1.0) {
        toSet = 1.0;
    }
    
    _teamProgress.value = toSet;
    [_teamProgress setNeedsDisplay];
    
    
    [_personalGoalLabel setText:[formatter stringForObjectValue:[NSNumber numberWithInt:(int)(personalGoal)]]];
    [_personalRaisedLabel setText:[formatter stringForObjectValue:[NSNumber numberWithInt:(int)(personalRaised)]]];
    
    toSet = (personalRaised/personalGoal);
    if (toSet > 1.0) {
        toSet = 1.0;
    }
    _personalProgress.value = toSet;
    [_personalProgress setNeedsDisplay];


    [_donorsTableView reloadData];
    
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    NSString *cellIdentifier = @"donorReuseID";
    TerpThonTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    /*
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier];
    }*/
    
    NSMutableDictionary *donationDictionary  = [donorsDataSource objectAtIndex:indexPath.item];

    
    NSNumber *currencyNumber = [NSNumber numberWithInt:[[donationDictionary objectForKey:@"donationamount"] intValue]];
    NSString *convertNumber = [formatter stringForObjectValue:currencyNumber];
    
    cell.subLabel.text = [[donationDictionary objectForKey:@"donationdateentered"] substringWithRange:NSMakeRange(5, 5)];
    cell.nameLabel.text = [NSString stringWithFormat:@"%@ %@",[donationDictionary objectForKey:@"donorfirstname"], [donationDictionary objectForKey:@"donorlastname"]];
    cell.moneyLabel.text = convertNumber;

    
    return cell;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (donorsDataSource)
    {
        return [donorsDataSource count];
    }
    else
    {
        return 0;
    }
}

@end
