//
//  HomeViewController.m
//  Terp Thon
//
//  Created by Tyler Lundfelt on 1/10/14.
//  Copyright (c) 2014 Tyler Lundfelt. All rights reserved.
//

#import "AppDataContainer.h"
#import "HomeViewController.h"
#import "AuxUIWebViewController.h"

@interface HomeViewController ()
{
    AppDataContainer *data;
}
@end

@implementation HomeViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    //Set up the app's data source by HTTP requesting the server
    self.view.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"triangular.png"]];
    [self refresh:nil];
}

-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:YES];
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender{
    
    if ([segue.identifier isEqualToString:@"settingsSegue"])
    {
        return;
    }
    
    AuxUIWebViewController *controller = (AuxUIWebViewController *)segue.destinationViewController;
    controller.contentType = @"";
    controller.contentBundle = nil;
        
    if([segue.identifier isEqualToString:@"donateSegue"])
    {
        NSString *to_append = @"";
        if (data.participant_ID) {
            to_append = data.participant_ID;
        }
        
        controller.contentName = [@"http://www.helpmakemiracles.org/index.cfm?fuseaction=donorDrive.participant&participantID=" stringByAppendingString:to_append];
    }
    else if ([segue.identifier isEqualToString:@"eventPageSegue"])
    {
        controller.contentName = @"http://www.helpmakemiracles.org/event/terpthon14";
    }
    else if ([segue.identifier isEqualToString:@"websiteSegue"])
    {
            controller.contentName = @"http://www.terpthon.org";
    }
    
    
}

-(IBAction)refresh:(id)sender
{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    
    if (![defaults boolForKey:@"has_onboarded"]) {
        
        UIAlertView *onboard_alert = [[UIAlertView alloc] initWithTitle:@"Welcome!" message:@"Click the gear icon to the left to enter your DancersID.\n Use the refresh arrow icon to the right to reload data. Data is updated every 15 minutes." delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil];
        [onboard_alert show];
        
        [defaults setBool:YES forKey:@"has_onboarded"];
    }
    
    data = [AppDataContainer getInstance];
    data.participant_ID = [defaults stringForKey:@"participant_id"];
    [data getDataFromParticipantID];
}

@end
